//
//  patientProfileViewController.swift
//  HCI project
//
//  Created by Mufaza Majeed on 11/25/19.
//  Copyright © 2019 Mufaza Majeed. All rights reserved.
//

import Foundation
import UIKit
class patientProfileViewController: UIViewController {

    @IBOutlet var segmentedControll: UISegmentedControl!
    @IBOutlet var topView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var clinicalScrollView: UIScrollView!
    @IBOutlet var infoTopView: UIView!
    @IBOutlet var followview1: UIView!
    @IBOutlet var followView2: UIView!
    @IBOutlet var followView3: UIView!
    @IBOutlet var infoBottomView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topView.layer.borderWidth = 1.0
        topView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        bottomView.layer.borderWidth = 1.0
        bottomView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        infoTopView.layer.borderWidth = 1.0
        infoTopView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        followview1.layer.borderWidth = 1.0
        followview1.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        followView2.layer.borderWidth = 1.0
        followView2.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        followView3.layer.borderWidth = 1.0
        followView3.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        // Do any additional setup after loading the view.
    }
    @IBAction func openGallery(_ sender: Any) {
        
        var picker = UIImagePickerController();
        self.present(picker, animated: true, completion: nil)
    }
    
    @IBAction func segmentedValueChange(_ sender: UISegmentedControl) {
        
        if segmentedControll.selectedSegmentIndex == 0 {
           topView.isHidden = true
            bottomView.isHidden = true
            infoTopView.isHidden = false
            infoBottomView.isHidden = false
        } else {
            topView.isHidden = false
            bottomView.isHidden = false
            infoTopView.isHidden = true
            infoBottomView.isHidden = true
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

